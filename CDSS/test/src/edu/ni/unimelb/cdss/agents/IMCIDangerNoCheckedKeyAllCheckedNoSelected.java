package edu.ni.unimelb.cdss.agents;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.jayway.android.robotium.solo.Solo;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity;
import edu.ni.unimelb.cdss.diagnosis.KeySymptomsActivity;
import edu.ni.unimelb.cdss.diagnosis.QuestionsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity;
import edu.ni.unimelb.cdss.patientInfo.PTempPAgeActivity;

public class IMCIDangerNoCheckedKeyAllCheckedNoSelected extends ActivityInstrumentationTestCase2<PTempPAgeActivity> {

	private Solo solo;
	private final int REPEAT = 1;

	public IMCIDangerNoCheckedKeyAllCheckedNoSelected() {
		super(PTempPAgeActivity.class);
	}

	@Override
	public void setUp() throws Exception {
		// setUp() is run before a test case is started.
		// This is where the solo object is created.
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@Override
	public void tearDown() throws Exception {
		// tearDown() is run after a test case has finished.
		// finishOpenedActivities() will finish all the activities that have
		// been opened during the test execution.

		solo.finishOpenedActivities();
	}

	public void testRepeater() throws Exception {

		// set age
		solo.clickOnView(solo.getView(R.id.date_of_birth_button_ptemp_page));
		solo.setDatePicker(0, 2012, 7, 24);
		solo.clickOnText("Set");

		for (int i = 1; i <= REPEAT; i++) {
			Log.v("REPEAT", "Test iteration number " + i);
			dangerNoCheckedKeyAllCheckedNoSelected();
		}
	}

	public void dangerNoCheckedKeyAllCheckedNoSelected() throws Exception {

		solo.clickOnButton("Next");

		solo.waitForActivity(DangerSignsActivity.class);

		// ///////////////////
		// Assert that TemperatureActivity -> DangerSignsActivity
		solo.assertCurrentActivity("Expected DangerSignsActivity", "DangerSignsActivity");

		solo.clickOnButton("Next");

		solo.waitForActivity(KeySymptomsActivity.class);

		// ///////////////////
		// Assert that DangerSignsActivity -> KeySymptomsActivity
		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		solo.clickOnView(solo.getView(R.id.switchKeyCough));
		solo.clickOnView(solo.getView(R.id.switchKeyBreath));
		solo.clickOnView(solo.getView(R.id.switchKeyDiarrhoea));
		solo.clickOnView(solo.getView(R.id.switchKeyFever));
		solo.clickOnView(solo.getView(R.id.switchKeyEar));

		solo.clickOnButton("Next");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnButton("Next");

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.no));

		solo.waitForActivity(RecommendationsActivity.class);
		// ////////////////////
		// Assert that QuestionsActivity -> RecommendationsActivity
		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		solo.goBack();

		// ///////////////////
		// Assert that RecommendationsActivity -> PTempPAgeActivity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");

		Log.d("ROBOTIUM", "Current activity at: " + solo.getCurrentActivity().toString());
	}
}
