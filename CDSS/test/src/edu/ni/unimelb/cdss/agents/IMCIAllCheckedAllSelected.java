package edu.ni.unimelb.cdss.agents;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.jayway.android.robotium.solo.Solo;

import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity;
import edu.ni.unimelb.cdss.diagnosis.KeySymptomsActivity;
import edu.ni.unimelb.cdss.diagnosis.QuestionsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsActivity;
import edu.ni.unimelb.cdss.diagnosis.RecommendationsDialog;
import edu.ni.unimelb.cdss.patientInfo.PTempPAgeActivity;

public class IMCIAllCheckedAllSelected extends ActivityInstrumentationTestCase2<PTempPAgeActivity> {

	private Solo solo;
	private final int REPEAT = 1;

	public IMCIAllCheckedAllSelected() {
		super(PTempPAgeActivity.class);
	}

	@Override
	public void setUp() throws Exception {
		// setUp() is run before a test case is started.
		// This is where the solo object is created.
		solo = new Solo(getInstrumentation(), getActivity());
	}

	@Override
	public void tearDown() throws Exception {
		// tearDown() is run after a test case has finished.
		// finishOpenedActivities() will finish all the activities that have
		// been opened during the test execution.

		solo.finishOpenedActivities();
	}

	public void testRepeater() throws Exception {

		// set age
		solo.clickOnView(solo.getView(R.id.date_of_birth_button_ptemp_page));
		solo.setDatePicker(0, 2012, 7, 24);
		solo.clickOnText("Set");

		for (int i = 1; i <= REPEAT; i++) {
			Log.v("REPEAT", "Test iteration number " + i);
			allCheckedAllSelected();
		}
	}

	public void allCheckedAllSelected() throws Exception {

		solo.clickOnButton("Next");

		solo.waitForActivity(DangerSignsActivity.class);

		// ///////////////////
		// Assert that TemperatureActivity -> DangerSignsActivity
		solo.assertCurrentActivity("Expected DangerSignsActivity", "DangerSignsActivity");

		solo.clickOnView(solo.getView(R.id.switchDangerDrink));
		solo.clickOnView(solo.getView(R.id.switchDangerVomit));
		solo.clickOnView(solo.getView(R.id.switchDangerConvulsed));
		solo.clickOnView(solo.getView(R.id.switchDangerLethUncon));
		solo.clickOnView(solo.getView(R.id.switchDangerConvulsing));

		solo.clickOnButton("Next");

		solo.waitForActivity(KeySymptomsActivity.class);

		// ///////////////////
		// Assert that DangerSignsActivity -> KeySymptomsActivity
		solo.assertCurrentActivity("Expected KeySymptomsActivity", "KeySymptomsActivity");

		solo.clickOnView(solo.getView(R.id.switchKeyCough));
		solo.clickOnView(solo.getView(R.id.switchKeyBreath));
		solo.clickOnView(solo.getView(R.id.switchKeyDiarrhoea));
		solo.clickOnView(solo.getView(R.id.switchKeyFever));
		solo.clickOnView(solo.getView(R.id.switchKeyEar));

		solo.clickOnButton("Next");

		// Random random = new Random();

		// random.setSeed((new Date()).getTime());
		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		// 2
		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.waitForActivity(RecommendationsDialog.class);

		solo.assertCurrentActivity("Expected RecommendationsDialog", "RecommendationsDialog");

		solo.clickOnButton("Continue");

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(QuestionsActivity.class);

		solo.assertCurrentActivity("Expected QuestionsActivity", "QuestionsActivity");

		solo.clickOnView(solo.getView(R.id.yes));

		solo.waitForActivity(RecommendationsActivity.class);
		// ////////////////////
		// Assert that QuestionsActivity -> RecommendationsActivity
		solo.assertCurrentActivity("Expected RecommendationsActivity", "RecommendationsActivity");

		solo.goBack();

		// ///////////////////
		// Assert that RecommendationsActivity -> TemperatureActivity
		solo.assertCurrentActivity("Expected PTempPAgeActivity", "PTempPAgeActivity");

		Log.d("ROBOTIUM", "Current activity at: " + solo.getCurrentActivity().toString());
	}
}
