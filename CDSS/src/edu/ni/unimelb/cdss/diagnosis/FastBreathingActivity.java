package edu.ni.unimelb.cdss.diagnosis;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
//import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
import edu.ni.unimelb.cdss.numberPicker.NumberPickerFastBreathing;

/**
 * This activity to get patient breathing rate.
 * 
 * @author Ilhamy Putra
 * @version 0.3
 */
public class FastBreathingActivity extends Activity implements ActivityListener {

	/**
	 * Context reference
	 */
	final Context context = this;

	/**
	 * Next Button reference
	 */
	protected Button btnNext;

	/**
	 * Skip Button reference
	 */
	protected Button btnSkip;

	/**
	 * Help Button reference
	 */
	protected ImageButton btnHelp;

	/**
	 * Button pressed flag
	 */
	protected boolean pressedFlag;

	/**
	 * Question text view reference
	 */
	public TextView question;

	/**
	 * Progress reference
	 */
	ProgressDialog progressDialog;
	ImageButton mHomeButton;
	ImageButton mBackButton;

	/**
	 * Number picker reference
	 */
	private NumberPickerFastBreathing numberPicker;

	/**
	 * Help ImageView for recycling
	 */
	ImageView image;

	/**
	 * Application reference
	 */
	private ApplicationController app;

	public FastBreathingActivity() {

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fast_breathing_view);

		// initialisation
		Static.currentActivity = this;
		app = ((ApplicationController) getApplicationContext());

		question = (TextView) findViewById(R.id.questions);
		if (Static.currentQuestion != null) {
			System.out.println(Static.currentQuestion.getQuestionString());
			question.setText(Static.currentQuestion.getQuestionString());
		}
		numberPicker = (NumberPickerFastBreathing) findViewById(R.id.picker);

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonFast);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				//Intent intent = new Intent(view.getContext(), HomeActivity.class);
				//startActivity(intent);
			}
		});

		mBackButton = (ImageButton) findViewById(R.id.backButtonFast);
		mBackButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

		// next button listener
		btnNext = (Button) findViewById(R.id.next);
		btnNext.setTextSize(18);
		btnNext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// clear image
				if (image != null) {
					Drawable drawable = image.getDrawable();
					if (drawable instanceof BitmapDrawable) {
						BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
						Bitmap bitmap = bitmapDrawable.getBitmap();
						bitmap.recycle();
					}
				}

				btnNext.setEnabled(false);
				btnHelp.setEnabled(false);
				btnSkip.setEnabled(false);
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onNextClick();
						}
					};
					thread.start();
				}
			}
		});

		// skip button listener
		btnSkip = (Button) findViewById(R.id.skip);
		btnSkip.setTextSize(18);
		btnSkip.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				btnNext.setEnabled(false);
				btnHelp.setEnabled(false);
				btnSkip.setEnabled(false);
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onSkipClick();
						}
					};
					thread.start();

				}
			}
		});

		// Help button listener
		btnHelp = (ImageButton) findViewById(R.id.help);
		// btnHelp.setTextSize(18);
		btnHelp.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				btnNext.setEnabled(false);
				btnHelp.setEnabled(false);
				btnSkip.setEnabled(false);

				final Dialog dialog = new Dialog(context, R.style.AppBaseTheme);
				String qText = new String(question.getText().toString());

				if (getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
						getPackageName()) == 0) {
					// no picture
					/**
					 * The below arguments will be set by using getHelpContent
					 * and getQuestionTerms, when the data becomes available
					 */
					dialog.setContentView(R.layout.custom_help_dialog_no_picture);

					TextView title = (TextView) dialog.findViewById(R.id.titleHelpImg);
					title.setText(qText);
					TextView helpText = (TextView) dialog.findViewById(R.id.helpText);
					helpText.setText(Static.currentQuestion.getQuestionHelp());
					Button dialogButton = (Button) dialog.findViewById(R.id.closeBtn);
					dialogButton.setTextSize(18);
					// if button is clicked, close the custom dialog
					dialogButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
							btnNext.setEnabled(true);
							btnHelp.setEnabled(true);
							btnSkip.setEnabled(true);
						}
					});
					dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							dialog.dismiss();
							btnNext.setEnabled(true);
							btnHelp.setEnabled(true);
							btnSkip.setEnabled(true);
						}
					});
					dialog.show();
				} else {
					/**
					 * The below arguments will be set by using getPictureID,
					 * getHelpContent and getQuestionTerms, when the data
					 * becomes available
					 */
					dialog.setContentView(R.layout.custom_help_dialog_with_picture);

					TextView title = (TextView) dialog.findViewById(R.id.titleHelpImg);
					title.setText(qText);
					image = (ImageView) dialog.findViewById(R.id.helpImg);
					image.setImageResource(getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(),
							"drawable", getPackageName()));
					image.setOnLongClickListener(new OnLongClickListener() {
						@Override
						public boolean onLongClick(View v) {
							Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
							intent.putExtra("details_filename", Static.currentQuestion.getQuestionHelpImage());
							startActivity(intent);
							overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
							return true;
						}
					});

					TextView helpText = (TextView) dialog.findViewById(R.id.helpText);
					helpText.setText(Static.currentQuestion.getQuestionHelp());
					Button dialogButton = (Button) dialog.findViewById(R.id.closeBtn);
					dialogButton.setTextSize(18);
					// if button is clicked, close the custom dialog
					dialogButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
							btnNext.setEnabled(true);
							btnHelp.setEnabled(true);
							btnSkip.setEnabled(true);
						}
					});
					dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							dialog.dismiss();
							btnNext.setEnabled(true);
							btnHelp.setEnabled(true);
							btnSkip.setEnabled(true);
						}
					});

					dialog.show();
				}
			}
		});
	}

	/**
	 * Add feature answer to environment.
	 */
	protected void onNextClick() {
		int age = 60;
		if (app.getpAge_years().length() > 0) {
			if (app.getpAge_months().length() > 0) {
				age = Integer.parseInt(app.getpAge_years()) * 12 + Integer.parseInt(app.getpAge_months());
			} else {
				age = Integer.parseInt(app.getpAge_years()) * 12;
			}
		}

		if (((age < 13) && (numberPicker.getValue() >= 50)) || ((age >= 13) && (numberPicker.getValue() >= 40))) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
					ASSyntax.createAtom(Static.currentQuestion.getFeatureCode()), ASSyntax.createAtom("yes")));
		} else {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
					ASSyntax.createAtom(Static.currentQuestion.getFeatureCode()), ASSyntax.createAtom("no")));
		}
		Static.environment.addPercept(Literal.parseLiteral("ask"));
	}

	/**
	 * Add feature answer as skip to environment.
	 */
	protected void onSkipClick() {
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom(Static.currentQuestion.getFeatureCode()), ASSyntax.createAtom("na")));
		Static.environment.addPercept(Literal.parseLiteral("ask"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener#next(java.lang
	 * .String)
	 */
	@Override
	public void next(String activity) {
		try {
			if (activity.equals(Static.RECOMMENDATION_DIALOG)) {
				startActivity(new Intent(this, Class.forName(activity)));
			} else {
				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				startActivity(new Intent(this, Class.forName(activity)));
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Static.currentActivity = this;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			//Intent exit = new Intent(this, HomeActivity.class);
			//exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//exit.putExtra("EXIT", true);
			//startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
