package edu.ni.unimelb.cdss.diagnosis;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import edu.ni.unimelb.cdss.R;
//import edu.ni.unimelb.cdss.control.HomeActivity;

/**
 * @version 0.3
 * 
 * 
 */
public class OutcomesActivity extends Activity {

	private Button mTreatment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.outcomes_view);

		mTreatment = (Button) findViewById(R.id.go_to_treat);
		mTreatment.setOnClickListener(new TratButtonListener());
	}

	class TratButtonListener implements View.OnClickListener {

		@Override
		public void onClick(View v) {

			// change this listener later based on your needs
			Intent to_treat = new Intent(OutcomesActivity.this, RecommendationsActivity.class);
			startActivity(to_treat);
		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			//Intent exit = new Intent(this, HomeActivity.class);
			//exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//exit.putExtra("EXIT", true);
			//startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
