package edu.ni.unimelb.cdss.diagnosis;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
//import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
import edu.ni.unimelb.cdss.guiutils.Switch;

/**
 * A screen presenting a list of checkbox questions to cover the key symptoms of
 * the IMCI.
 * 
 * @author ilhamyputra
 * @version 0.3
 */
public class KeySymptomsActivity extends Activity implements ActivityListener {

	ImageButton mHomeButton;
	ImageButton mBackbutton;
	/**
	 * Next Button reference
	 */
	protected Button btnNext;

	/**
	 * Button pressed flag
	 */
	boolean pressedFlag;

	/**
	 * Cough MySwitch reference
	 */
	private Switch switchCough;

	/**
	 * Breathing MySwitch reference
	 */
	private Switch switchBreath;

	/**
	 * Diarrhoea MySwitch reference
	 */
	private Switch switchDiarrhoea;

	/**
	 * Fever MySwitch reference
	 */
	private Switch switchFever;

	/**
	 * Ear problems MySwitch reference
	 */
	private Switch switchEar;

	/**
	 * Progress reference
	 */
	private ProgressDialog progressDialog;

	/**
	 * Application reference
	 */
	private ApplicationController app;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.key_symptoms_view);
		mHomeButton = (ImageButton) findViewById(R.id.homeButtonKeyS);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				//Intent intent = new Intent(view.getContext(), HomeActivity.class);
				//startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonKeyS);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});
		// initialisation
		Static.currentActivity = this;
		pressedFlag = false;
		app = ((ApplicationController) getApplicationContext());

		switchCough = (Switch) findViewById(R.id.switchKeyCough);
		switchBreath = (Switch) findViewById(R.id.switchKeyBreath);
		switchDiarrhoea = (Switch) findViewById(R.id.switchKeyDiarrhoea);
		switchFever = (Switch) findViewById(R.id.switchKeyFever);
		switchEar = (Switch) findViewById(R.id.switchKeyEar);

		if (app.getPTemp().length() > 0) {
			Double temperature = Double.parseDouble(app.getPTemp());
			if (temperature >= 37.5) {
				switchFever.setChecked(true);
			}
		}

		// next button listener
		btnNext = (Button) findViewById(R.id.next4);
		btnNext.setTextSize(18);
		btnNext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onNextClick();
						}
					};
					thread.start();
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			//Intent exit = new Intent(this, HomeActivity.class);
			//exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//exit.putExtra("EXIT", true);
			//startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener#next(java.lang
	 * .String)
	 */
	@Override
	public void next(String activity) {
		try {
			if (activity.equals(Static.RECOMMENDATION_DIALOG)) {
				startActivity(new Intent(this, Class.forName(activity)));
			} else {
				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				startActivity(new Intent(this, Class.forName(activity)));
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Called when next button clicked. It calls addKeySymptomsBelief().
	 * 
	 * @see edu.ni.unimelb.cdss.diagnosis.KeySymptomsActivity#addKeySymptomsBelief()
	 */
	protected void onNextClick() {
		addKeySymptomsBelief();
	}

	/**
	 * Add key symptoms belief to environment.
	 */
	public void addKeySymptomsBelief() {
		if (switchBreath.isChecked() || switchCough.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("coughing_or_difficult_breathing"), ASSyntax.createAtom("yes")));
		} else if (!switchBreath.isChecked() && !switchCough.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("coughing_or_difficult_breathing"), ASSyntax.createAtom("no")));
		}
		if (switchDiarrhoea.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("diarrhoea"), ASSyntax.createAtom("yes")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dehydration"), ASSyntax.createAtom("yes")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dysentery"), ASSyntax.createAtom("yes")));
		} else if (!switchDiarrhoea.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("diarrhoea"), ASSyntax.createAtom("no")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dehydration"), ASSyntax.createAtom("no")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dysentery"), ASSyntax.createAtom("no")));
		}
		if (switchEar.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("ear_problems"), ASSyntax.createAtom("yes")));
		} else if (!switchEar.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("ear_problems"), ASSyntax.createAtom("no")));
		}
		if (switchFever.isChecked()) {

			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("fever_malaria"), ASSyntax.createAtom("yes")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("fever_measles"), ASSyntax.createAtom("yes")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dengue_fever"), ASSyntax.createAtom("yes")));

		} else if (!switchFever.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("fever_malaria"), ASSyntax.createAtom("no")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("fever_measles"), ASSyntax.createAtom("no")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dengue_fever"), ASSyntax.createAtom("no")));

		}
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "ask"));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

}
