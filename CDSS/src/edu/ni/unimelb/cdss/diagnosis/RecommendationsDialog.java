package edu.ni.unimelb.cdss.diagnosis;

import jason.asSyntax.ListTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.diagnosis.recommendation.OutcomeRecommendation;
import edu.ni.unimelb.cdss.diagnosis.recommendation.Recommendation;
import edu.ni.unimelb.cdss.diagnosis.recommendation.RecommendationComparator;
import edu.ni.unimelb.cdss.diagnosis.recommendation.RecommendationFactory;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
//import edu.ni.unimelb.cdss.patientInfo.PatientInfo;

/**
 * Show outcome and treatment details.
 * 
 * Receive list of outcome and recommendation from user interaction agent.
 * 
 * @author Nisix Team
 * @author reused and refined by ilhamyputra
 * @version 0.3
 * 
 */
public class RecommendationsDialog extends Activity {

	private LinearLayout listOuts;
	private LinearLayout listTreats;
	protected Button btnNewPatient;
	protected Button btnDone;

//	private PatientInfo patient;
	private String pTemp;
	private ApplicationController app;

	private Button mDoneButton;
	private Button mNewPatientButton;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recommendations_dialog_view);

		// Set up the list views
		listOuts = (LinearLayout) findViewById(R.id.listOuts);
		listTreats = (LinearLayout) findViewById(R.id.listTreats);

		// Load the saved recommendation string
		buildRecommendations(Static.recommendation);

		// end by ilhamy

		/*
		 * ::::::::::::::::::::::::::::::::::::::::::::::: This is added by Eman
		 * K to insert the whole record of patient information.
		 */
		app = ((ApplicationController) getApplicationContext());
//		patient = app.getPatient();
		pTemp = app.getPTemp();
		mDoneButton = (Button) findViewById(R.id.doneButton);
		// start by ilhamy
		mDoneButton.setTextSize(18);
		// end by ilhamy
		mDoneButton.setOnClickListener(new DoneButtonListener());

		mNewPatientButton = (Button) findViewById(R.id.continueButton);
		// start by ilhamy
		mNewPatientButton.setTextSize(18);
		// end by ilhamy
		mNewPatientButton.setOnClickListener(new ContinueButtonListener());

		/*
		 * :::::::::::::::::::::::::::::::::::::::::::::::
		 */
	}

	class ContinueButtonListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			finish();
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		}

	}

	class DoneButtonListener implements View.OnClickListener {

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			/*
			 * Add diagnosis text into this object by "patient.set..."
			 */
			((Activity) Static.currentActivity).finish();
			Static.idRecommendationItem = R.layout.recommendation_item;
			startActivity(new Intent(v.getContext(), RecommendationsActivity.class));
			finish();
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			/*
			 * if(patient.insertPatient(getApplicationContext(),patient)){
			 * Toast.makeText(RecommendationsDialog.this, R.string.save_toast,
			 * Toast.LENGTH_SHORT).show(); } else
			 * Toast.makeText(RecommendationsDialog.this,
			 * R.string.Notsave_toast, Toast.LENGTH_SHORT).show();
			 */
		}

	}

	/**
	 * Convert the recommendations structure into a usable format.
	 * 
	 * @param recsStruct
	 *            The Jason structure of all reported outcomes and treatments.
	 */
	private void buildRecommendations(Structure recsStruct) {
		// TODO Refactor this common logic
		// Create a comparator for the recommendations
		RecommendationComparator recCmp = new RecommendationComparator();
		// Outcomes first
		// Has the items to remove duplicates
		Set<Recommendation> outSet = new HashSet<Recommendation>();
		Structure outcomes = (Structure) (recsStruct.getTerm(0));
		// Add each item
		ListTerm outcomeList = (ListTerm) (outcomes.getTerm(0));
		for (Term outcomeTerm : outcomeList) {
			ListTerm outcomeStatus = (ListTerm) (outcomeTerm);
			// String outcomeName = ((StringTerm) outcomeTerm).getString();
			outSet.add(new OutcomeRecommendation(outcomeStatus));
		}
		// Take the hash map and add it all to an ordered set
		Set<Recommendation> outTree = new TreeSet<Recommendation>(recCmp);
		outTree.addAll(outSet);
		for (final Recommendation outcome : outTree) {
			View view = outcome.createView(getApplicationContext());
			listOuts.addView(view);
			// Handle the clicks on the individual recommendation items
			view.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Log.d("RECOMMENDATION_OUTCOME", "Clicked on " + outcome.getLabel());
					onRecommendationClick(outcome);
				}
			});
		}

		// Treatments second
		// TODO Reuse code above
		Set<Recommendation> treatSet = new HashSet<Recommendation>();
		Structure treatments = (Structure) (recsStruct.getTerm(1));
		ListTerm treatmentList = (ListTerm) (treatments.getTerm(0));
		for (Term treatmentTerm : treatmentList) {
			Structure treatmentStruct = (Structure) treatmentTerm;
			treatSet.add(RecommendationFactory.createTreatmentRecommendation(treatmentStruct));
		}
		Set<Recommendation> treatTree = new TreeSet<Recommendation>(recCmp);
		treatTree.addAll(treatSet);
		for (final Recommendation treatment : treatTree) {
			View view = treatment.createView(getApplicationContext());
			listTreats.addView(view);
			// Handle the clicks on the individual recommendation items
			view.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Log.d("RECOMMENDATION_TREATMENT", "Clicked on " + treatment.getLabel());
					onRecommendationClick(treatment);
				}
			});
		}
	}

	/**
	 * Handle the user clicking on a recommendation item from the list. If there
	 * is an associated image to display, load it up.
	 * 
	 * @param rec
	 *            The recommendation that was clicked.
	 */
	protected void onRecommendationClick(Recommendation rec) {
		// If the recommendation has extra details to display
		if (rec.getDetailsFilename() != null) {
			// Switch to the details image view
			Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
			intent.putExtra("details_filename", rec.getDetailsFilename());
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		}
	}
}
