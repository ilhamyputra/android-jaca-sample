package edu.ni.unimelb.cdss.diagnosis;

//import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import edu.ni.unimelb.cdss.R;
//import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.imagehandler.ImageViewTouch;

/**
 * Provides the user with a scrollable, zoomable image providing further detail
 * information about something, such as a question or recommendation item.
 * 
 * Expects the filename of the image to display to be passed in as a string in
 * the extra message of the intent under the key "details_filename".
 * 
 * @version 0.3
 */
public class TreatmentDetailsActivity extends Activity {

	Bitmap mBitmap;
	ImageViewTouch mImage;

	/**
	 * Set up the activity and load the detail image.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.treatment_details);

		// Get the message from the intent
		String message = getIntent().getStringExtra("details_filename");

		// Create the image
		BitmapFactory.Options opts = new BitmapFactory.Options();

		mBitmap = BitmapFactory.decodeResource(getResources(),
				getResources().getIdentifier(message, "drawable", getPackageName()), opts);

		// Set the image
		mImage = (ImageViewTouch) findViewById(R.id.image);
		mImage.setImageBitmap(decodeSampledBitmapFromResource(getResources(),
				getResources().getIdentifier(message, "drawable", getPackageName()), 600, 600));

		Button dialogButton = (Button) findViewById(R.id.closeBtn);
		dialogButton.setTextSize(18);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mBitmap.recycle();
	}

	private static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

		// First decode with inJustDecodeBounds=true to check dimensions
		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(res, resId, options);

		// Calculate inSampleSize
		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

		// Decode bitmap with inSampleSize set
		options.inJustDecodeBounds = false;

		options.inDither = false; // Disable Dithering mode
		options.inPurgeable = true; // Tell to gc that whether it needs free
									// memory, the Bitmap can be cleared
		// Which kind of reference will be used to recover the Bitmap data after
		// being clear, when it will be used in the future
		options.inInputShareable = true;
		options.inTempStorage = new byte[32 * 1024];

		return BitmapFactory.decodeResource(res, resId, options);
	}

	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			// Calculate ratios of height and width to requested height and
			// width
			final int heightRatio = Math.round((float) height / (float) reqHeight);
			final int widthRatio = Math.round((float) width / (float) reqWidth);

			// Choose the smallest ratio as inSampleSize value, this will
			// guarantee
			// a final image with both dimensions larger than or equal to the
			// requested height and width.
			inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
		}

		return inSampleSize;
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
//			Intent exit = new Intent(this, HomeActivity.class);
//			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			exit.putExtra("EXIT", true);
//			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}
}
