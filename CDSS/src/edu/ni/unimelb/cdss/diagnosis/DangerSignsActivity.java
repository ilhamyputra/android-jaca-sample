package edu.ni.unimelb.cdss.diagnosis;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import edu.ni.unimelb.cdss.R;
//import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
import edu.ni.unimelb.cdss.diagnosis.utils.JasonUtils;
import edu.ni.unimelb.cdss.guiutils.Switch;

/**
 * A screen presenting a list of checkbox questions to cover the danger signs of
 * the IMCI.
 * 
 * @author ilhamyputra
 * @version 0.3
 * 
 */
public class DangerSignsActivity extends Activity implements ActivityListener {

	ImageButton mHomeButton;
	ImageButton mBackbutton;
	/**
	 * Next Button reference
	 */
	protected Button btnNext;

	/**
	 * Button pressed flag
	 */
	boolean pressedFlag;

	/**
	 * Drink MySwitch reference
	 */
	private Switch switchDrink;

	/**
	 * Vomit MySwitch reference
	 */
	private Switch switchVomit;

	/**
	 * Convulsing MySwitch reference
	 */
	private Switch switchConvulsing;

	/**
	 * Lethargic or Unconscious MySwitch reference
	 */
	private Switch switchLethUncon;

	/**
	 * Convulsed MySwitch reference
	 */
	private Switch switchConvulsed;

	/**
	 * Progess reference
	 */
	ProgressDialog progressDialog;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.danger_signs_view);
		mHomeButton = (ImageButton) findViewById(R.id.homeButtonDang);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				//Intent intent = new Intent(view.getContext(), HomeActivity.class);
				//startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonDang);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});
		// initialisation
		Static.currentActivity = this;
		Static.currentQuestion = null;
		Static.recommendation = null;
		pressedFlag = false;

		switchDrink = (Switch) findViewById(R.id.switchDangerDrink);
		switchVomit = (Switch) findViewById(R.id.switchDangerVomit);
		switchConvulsed = (Switch) findViewById(R.id.switchDangerConvulsed);
		switchLethUncon = (Switch) findViewById(R.id.switchDangerLethUncon);
		switchConvulsing = (Switch) findViewById(R.id.switchDangerConvulsing);

		// next button listener
		btnNext = (Button) findViewById(R.id.next3);
		btnNext.setTextSize(18);
		btnNext.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onNextClick();
						}
					};
					thread.start();
				}
			}
		});

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreateOptionsMenu(android.view.Menu)
	 */
	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			//Intent exit = new Intent(this, HomeActivity.class);
			//exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//exit.putExtra("EXIT", true);
			//startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener#next(java.lang
	 * .String)
	 */
	@Override
	public void next(String activity) {
		try {
			if (progressDialog.isShowing()) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			startActivity(new Intent(this, Class.forName(activity)));
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			finish();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Called when next button clicked. It calls addDangerSignsBelief().
	 * 
	 * @see edu.ni.unimelb.cdss.diagnosis.DangerSignsActivity#addDangerSignsBelief()
	 */
	protected void onNextClick() {
		addDangerSignsBelief();
	}

	/**
	 * Add danger signs belief to environment.
	 */
	public void addDangerSignsBelief() {
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom("kf_drinking_problems"), ASSyntax.createAtom(JasonUtils.isChecked(switchDrink))));
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom("kf_vomits_everything"), ASSyntax.createAtom(JasonUtils.isChecked(switchVomit))));
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom("kf_had_convulsions"), ASSyntax.createAtom(JasonUtils.isChecked(switchConvulsed))));
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom("kf_lethargic_or_unconscious"),
				ASSyntax.createAtom(JasonUtils.isChecked(switchLethUncon))));
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom("kf_convulsing"), ASSyntax.createAtom(JasonUtils.isChecked(switchConvulsing))));
		if (switchDrink.isChecked() || switchVomit.isChecked() || switchConvulsing.isChecked()
				|| switchLethUncon.isChecked() || switchConvulsed.isChecked()) {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
					ASSyntax.createAtom("dangersigns"), ASSyntax.createAtom("yes")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dangersigns"), ASSyntax.createAtom("yes")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "dangersigns",
					ASSyntax.createAtom("yes")));
		} else {
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
					ASSyntax.createAtom("dangersigns"), ASSyntax.createAtom("no")));
			Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "keysymptom",
					ASSyntax.createAtom("dangersigns"), ASSyntax.createAtom("no")));
			Static.environment
					.addPercept(ASSyntax.createLiteral(Literal.LPos, "dangersigns", ASSyntax.createAtom("no")));
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

}
