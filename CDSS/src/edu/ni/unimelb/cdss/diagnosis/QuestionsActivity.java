package edu.ni.unimelb.cdss.diagnosis;

import jason.asSyntax.ASSyntax;
import jason.asSyntax.Literal;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import edu.ni.unimelb.cdss.R;
//import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;

/**
 * Question behaviour.
 * 
 * Initially the questions activity is waiting for questions of required
 * features from user interaction agent.
 * 
 * A question will be displayed when questions are received from user
 * interaction agent.
 * 
 * Question can be answered as yes, no or skip.
 * 
 * @author ilhamyputra and Yossef Mostafa
 * @version 0.3
 * 
 */
public class QuestionsActivity extends Activity implements ActivityListener {
	/**
	 * Context reference
	 */
	final Context context = this;

	/**
	 * Yes Button reference
	 */
	protected ImageButton btnYes;

	/**
	 * No Button reference
	 */
	protected ImageButton btnNo;

	/**
	 * Skip Button reference
	 */
	protected Button btnSkip;

	/**
	 * Help Button reference
	 */
	protected ImageButton btnHelp;

	/**
	 * Button pressed flag
	 */
	protected boolean pressedFlag;

	/**
	 * Question text view reference
	 */
	public TextView question;

	/**
	 * Progress reference
	 */
	ProgressDialog progressDialog;
	ImageButton mHomeButton;
	ImageButton mBackbutton;

	/**
	 * Help ImageView for recycling
	 */
	ImageView image;
	ImageView imageMini;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// no image
		if (getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable", getPackageName()) == 0) {

			setContentView(R.layout.questions_view_no_picture);
		} else {

			setContentView(R.layout.questions_view_with_picture);
			imageMini = (ImageView) findViewById(R.id.questionhelpImg);
			imageMini.setImageResource(getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(),
					"drawable", getPackageName()));

			imageMini.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {
					btnYes.setEnabled(false);
					btnNo.setEnabled(false);
					btnSkip.setEnabled(false);
					btnHelp.setEnabled(false);

					final Dialog dialog = new Dialog(context, R.style.AppBaseTheme);
					String qText = new String(question.getText().toString());

					if (getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
							getPackageName()) == 0) {
						// no picture
						/**
						 * The below arguments will be set by using
						 * getHelpContent and getQuestionTerms, when the data
						 * becomes available
						 */
						dialog.setContentView(R.layout.custom_help_dialog_no_picture);

						TextView title = (TextView) dialog.findViewById(R.id.titleHelpImg);
						title.setText(qText);
						TextView helpText = (TextView) dialog.findViewById(R.id.helpText);
						helpText.setText(Static.currentQuestion.getQuestionHelp());
						Button dialogButton = (Button) dialog.findViewById(R.id.closeBtn);
						dialogButton.setTextSize(18);
						// if button is clicked, close the custom dialog
						dialogButton.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								dialog.dismiss();
								btnYes.setEnabled(true);
								btnNo.setEnabled(true);
								btnSkip.setEnabled(true);
								btnHelp.setEnabled(true);
							}
						});
						dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {
								dialog.dismiss();
								btnYes.setEnabled(true);
								btnNo.setEnabled(true);
								btnSkip.setEnabled(true);
								btnHelp.setEnabled(true);
							}
						});
						dialog.show();
					} else {
						/**
						 * The below arguments will be set by using
						 * getPictureID, getHelpContent and getQuestionTerms,
						 * when the data becomes available
						 */
						dialog.setContentView(R.layout.custom_help_dialog_with_picture);

						TextView title = (TextView) dialog.findViewById(R.id.titleHelpImg);
						title.setText(qText);
						image = (ImageView) dialog.findViewById(R.id.helpImg);
						image.setImageResource(getResources().getIdentifier(
								Static.currentQuestion.getQuestionHelpImage(), "drawable", getPackageName()));
						image.setOnLongClickListener(new OnLongClickListener() {
							@Override
							public boolean onLongClick(View v) {
								Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
								intent.putExtra("details_filename", Static.currentQuestion.getQuestionHelpImage());
								startActivity(intent);
								overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
								return true;
							}
						});

						image.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
								intent.putExtra("details_filename", Static.currentQuestion.getQuestionHelpImage());
								startActivity(intent);
								overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
							}
						});

						TextView helpText = (TextView) dialog.findViewById(R.id.helpText);
						helpText.setText(Static.currentQuestion.getQuestionHelp());
						Button dialogButton = (Button) dialog.findViewById(R.id.closeBtn);
						dialogButton.setTextSize(18);
						// if button is clicked, close the custom dialog
						dialogButton.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {

								dialog.dismiss();
								btnYes.setEnabled(true);
								btnNo.setEnabled(true);
								btnSkip.setEnabled(true);
								btnHelp.setEnabled(true);
							}
						});
						dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
							@Override
							public void onCancel(DialogInterface dialog) {

								dialog.dismiss();
								btnYes.setEnabled(true);
								btnNo.setEnabled(true);
								btnSkip.setEnabled(true);
								btnHelp.setEnabled(true);
							}
						});

						dialog.show();
					}
				}

			});

		}

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonQus);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				//Intent intent = new Intent(view.getContext(), HomeActivity.class);
				//startActivity(intent);
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonQus);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});
		// initialisation
		Static.currentActivity = this;

		question = (TextView) findViewById(R.id.questions);
		if (Static.currentQuestion != null) {
			System.out.println(Static.currentQuestion.getQuestionString());
			question.setText(Static.currentQuestion.getQuestionString());
		}

		// yes button listener
		btnYes = (ImageButton) findViewById(R.id.yes);
		// btnYes.setTextSize(18);
		btnYes.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				btnYes.setEnabled(false);
				btnNo.setEnabled(false);
				btnSkip.setEnabled(false);
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onYesClick();
						}
					};
					thread.start();

				}
			}
		});

		// no button listener
		btnNo = (ImageButton) findViewById(R.id.no);
		// btnNo.setTextSize(18);
		btnNo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				btnYes.setEnabled(false);
				btnNo.setEnabled(false);
				btnSkip.setEnabled(false);
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onNoClick();
						}
					};
					thread.start();

				}
			}
		});

		// skip button listener
		btnSkip = (Button) findViewById(R.id.skip);
		btnSkip.setTextSize(18);
		btnSkip.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				btnYes.setEnabled(false);
				btnNo.setEnabled(false);
				btnSkip.setEnabled(false);
				if (!pressedFlag) {
					pressedFlag = true;
					progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
					Thread thread = new Thread() {
						public void run() {
							onSkipClick();
						}
					};
					thread.start();

				}
			}
		});

		// Help button listener
		btnHelp = (ImageButton) findViewById(R.id.help);
		// btnHelp.setTextSize(18);
		btnHelp.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				btnYes.setEnabled(false);
				btnNo.setEnabled(false);
				btnSkip.setEnabled(false);
				btnHelp.setEnabled(false);

				final Dialog dialog = new Dialog(context, R.style.AppBaseTheme);
				String qText = new String(question.getText().toString());

				if (getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(), "drawable",
						getPackageName()) == 0) {
					// no picture
					/**
					 * The below arguments will be set by using getHelpContent
					 * and getQuestionTerms, when the data becomes available
					 */
					dialog.setContentView(R.layout.custom_help_dialog_no_picture);

					TextView title = (TextView) dialog.findViewById(R.id.titleHelpImg);
					title.setText(qText);
					TextView helpText = (TextView) dialog.findViewById(R.id.helpText);
					helpText.setText(Static.currentQuestion.getQuestionHelp());
					Button dialogButton = (Button) dialog.findViewById(R.id.closeBtn);
					dialogButton.setTextSize(18);
					// if button is clicked, close the custom dialog
					dialogButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							dialog.dismiss();
							btnYes.setEnabled(true);
							btnNo.setEnabled(true);
							btnSkip.setEnabled(true);
							btnHelp.setEnabled(true);
						}
					});
					dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {
							dialog.dismiss();
							btnYes.setEnabled(true);
							btnNo.setEnabled(true);
							btnSkip.setEnabled(true);
							btnHelp.setEnabled(true);
						}
					});
					dialog.show();
				} else {
					/**
					 * The below arguments will be set by using getPictureID,
					 * getHelpContent and getQuestionTerms, when the data
					 * becomes available
					 */
					dialog.setContentView(R.layout.custom_help_dialog_with_picture);

					TextView title = (TextView) dialog.findViewById(R.id.titleHelpImg);
					title.setText(qText);
					image = (ImageView) dialog.findViewById(R.id.helpImg);
					image.setImageResource(getResources().getIdentifier(Static.currentQuestion.getQuestionHelpImage(),
							"drawable", getPackageName()));
					image.setOnLongClickListener(new OnLongClickListener() {
						@Override
						public boolean onLongClick(View v) {
							Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
							intent.putExtra("details_filename", Static.currentQuestion.getQuestionHelpImage());
							startActivity(intent);
							overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
							return true;
						}
					});

					image.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {
							Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
							intent.putExtra("details_filename", Static.currentQuestion.getQuestionHelpImage());
							startActivity(intent);
							overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
						}
					});

					TextView helpText = (TextView) dialog.findViewById(R.id.helpText);
					helpText.setText(Static.currentQuestion.getQuestionHelp());
					Button dialogButton = (Button) dialog.findViewById(R.id.closeBtn);
					dialogButton.setTextSize(18);
					// if button is clicked, close the custom dialog
					dialogButton.setOnClickListener(new OnClickListener() {
						@Override
						public void onClick(View v) {

							dialog.dismiss();
							btnYes.setEnabled(true);
							btnNo.setEnabled(true);
							btnSkip.setEnabled(true);
							btnHelp.setEnabled(true);
						}
					});
					dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
						@Override
						public void onCancel(DialogInterface dialog) {

							dialog.dismiss();
							btnYes.setEnabled(true);
							btnNo.setEnabled(true);
							btnSkip.setEnabled(true);
							btnHelp.setEnabled(true);
						}
					});

					dialog.show();
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener#next(java.lang
	 * .String)
	 */
	@Override
	public void next(String activity) {
		try {
			if (activity.equals(Static.RECOMMENDATION_DIALOG)) {
				startActivity(new Intent(this, Class.forName(activity)));
			} else {
				if (progressDialog.isShowing()) {
					progressDialog.dismiss();
					progressDialog = null;
				}
				startActivity(new Intent(this, Class.forName(activity)));
				overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
				finish();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Add feature answer as yes to environment.
	 */
	protected void onYesClick() {
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom(Static.currentQuestion.getFeatureCode()), ASSyntax.createAtom("yes")));
		Static.environment.addPercept(Literal.parseLiteral("ask"));
	}

	/**
	 * Add feature answer as no to environment.
	 */
	protected void onNoClick() {
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom(Static.currentQuestion.getFeatureCode()), ASSyntax.createAtom("no")));
		Static.environment.addPercept(Literal.parseLiteral("ask"));
	}

	/**
	 * Add feature answer as skip to environment.
	 */
	protected void onSkipClick() {
		Static.environment.addPercept(ASSyntax.createLiteral(Literal.LPos, "answer",
				ASSyntax.createAtom(Static.currentQuestion.getFeatureCode()), ASSyntax.createAtom("na")));
		Static.environment.addPercept(Literal.parseLiteral("ask"));
	}

	@Override
	public void onResume() {
		super.onResume();
		Static.currentActivity = this;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			//Intent exit = new Intent(this, HomeActivity.class);
			//exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//exit.putExtra("EXIT", true);
			//startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
