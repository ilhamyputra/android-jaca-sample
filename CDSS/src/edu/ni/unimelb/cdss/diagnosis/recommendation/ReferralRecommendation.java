package edu.ni.unimelb.cdss.diagnosis.recommendation;

import edu.ni.unimelb.cdss.diagnosis.utils.Duration;



/**
 * Subclass of Recommendation recommending referral to another establishment.
 */
public class ReferralRecommendation extends Recommendation {

	public ReferralRecommendation(String where, Duration when, String why, Boolean urgent, String conditions) {
		super();

		String label = (!conditions.equals("") ? conditions + "\n" : "");
		label += "Refer" + (urgent ? " URGENTLY" : "") + " to " + where;
		label += !when.isEmpty() ? " within " + when : "";
		label += !why.equals("") ? " due to " + why : "";
		setLabel(label);
	}
}
