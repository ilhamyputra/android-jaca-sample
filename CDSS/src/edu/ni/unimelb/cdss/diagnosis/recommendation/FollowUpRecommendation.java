package edu.ni.unimelb.cdss.diagnosis.recommendation;

import edu.ni.unimelb.cdss.diagnosis.utils.Duration;



/**
 * Subclass of Recommendation recommending a follow-up after a given time.
 */
public class FollowUpRecommendation extends Recommendation {
	public FollowUpRecommendation(Duration date, String conditions) {
		super((!conditions.equals("") ? conditions + "\n" : "") + "Follow up in " + date);
	}
}
