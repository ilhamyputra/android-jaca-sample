package edu.ni.unimelb.cdss.diagnosis.utils;

/**
 * Convenience class for handling the fields of a duration with the confusion of
 * dates and calendars.
 * 
 * @author Nisix Team
 * @author reused by ilhamyputra for recommendation module
 * @version 0.3
 */
public class Duration {
	private int years;
	private int months;
	private int days;
	private int hours;
	private int minutes;
	private int seconds;

	/**
	 * Default constructor. Values default to zero.
	 */
	public Duration() {
	}

	/**
	 * Convenient constructor for passing in values.
	 */
	public Duration(int years, int months, int days, int hours, int minutes, int seconds) {
		setYears(years);
		setMonths(months);
		setDays(days);
		setHours(hours);
		setMinutes(minutes);
		setSeconds(seconds);
	}

	/**
	 * Serialise the duration values to a human-readable string.
	 */
	public String toString() {
		String output = "";
		if (getYears() != 0) {
			output += getYears() + " years, ";
		}
		if (getMonths() != 0) {
			output += getMonths() + " months, ";
		}
		if (getDays() != 0) {
			output += getDays() + " days, ";
		}
		if (getHours() != 0) {
			output += getHours() + " hours, ";
		}
		if (getMinutes() != 0) {
			output += getMinutes() + " minutes, ";
		}
		if (getSeconds() != 0) {
			output += getSeconds() + " seconds, ";
		}
		if (output.length() >= 2) {
			output = output.substring(0, output.length() - 2);
		}
		return output;
	}

	/**
	 * Whether or not all values in the duration are zero.
	 */
	public boolean isEmpty() {
		return getYears() == 0 && getMonths() == 0 && getDays() == 0 && getHours() == 0 && getMinutes() == 0
				&& getSeconds() == 0;
	}

	public int getYears() {
		return years;
	}

	public void setYears(int years) {
		this.years = years;
	}

	public int getMonths() {
		return months;
	}

	public void setMonths(int months) {
		this.months = months;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public int getHours() {
		return hours;
	}

	public void setHours(int hours) {
		this.hours = hours;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

}
