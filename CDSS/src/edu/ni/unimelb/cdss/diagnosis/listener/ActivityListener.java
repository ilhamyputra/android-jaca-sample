package edu.ni.unimelb.cdss.diagnosis.listener;

/**
 * Diagnosis activity listener.
 * 
 * @author ilhamyputra
 * @version 0.3
 * 
 */
public interface ActivityListener {
	/**
	 * Handle activity transition
	 * 
	 * @param activity
	 *            class name
	 */
	public void next(String activity);
}
