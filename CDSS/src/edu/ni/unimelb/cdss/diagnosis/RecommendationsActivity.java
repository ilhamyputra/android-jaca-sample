package edu.ni.unimelb.cdss.diagnosis;

import jaca.android.JaCaService;
import jason.asSyntax.ListTerm;
import jason.asSyntax.Structure;
import jason.asSyntax.Term;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
//import edu.ni.unimelb.cdss.control.HomeActivity;
import edu.ni.unimelb.cdss.diagnosis.recommendation.OutcomeRecommendation;
import edu.ni.unimelb.cdss.diagnosis.recommendation.Recommendation;
import edu.ni.unimelb.cdss.diagnosis.recommendation.RecommendationComparator;
import edu.ni.unimelb.cdss.diagnosis.recommendation.RecommendationFactory;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
//import edu.ni.unimelb.cdss.patientInfo.PatientInfo;
//import edu.ni.unimelb.cdss.qrcode.CameraActivity;

/**
 * Show outcome and treatment details.
 * 
 * Receive list of outcome and recommendation from user interaction agent.
 * 
 * @author Nisix Team
 * @author reused and refined by ilhamyputra
 * @version 0.3
 * 
 */
public class RecommendationsActivity extends Activity {

	private LinearLayout listOuts;
	private LinearLayout listTreats;
	protected Button btnNewPatient;
	protected Button btnSaveHist;

	//private PatientInfo patient;
	private String pTemp;

	private Button mSaveButton;
	ImageButton mHomeButton;
	ImageButton mBackbutton;

	private ApplicationController app;
//	private DiagnosisHistory history;

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recommendations_view);
		mHomeButton = (ImageButton) findViewById(R.id.homeButtonReco);
		mHomeButton.setOnClickListener(new NewPatButtonListener(this));

		mBackbutton = (ImageButton) findViewById(R.id.backButtonRec);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});
		// start by ilhamy
		Static.currentActivity = null;
		Static.environment = null;
		try {
			Intent service = new Intent(getApplicationContext(), JaCaService.class);
			service.putExtra(JaCaService.MAS2J, R.string.mas2j);
			getApplicationContext().stopService(service);
		} catch (RuntimeException e) {
		}
		Log.d("Activity", "MAS2J stopped!");

		// Set up the list views
		listOuts = (LinearLayout) findViewById(R.id.listOuts);
		listTreats = (LinearLayout) findViewById(R.id.listTreats);

		// Load the saved recommendation string
		buildRecommendations(Static.recommendation);

		// end by ilhamy

		/*
		 * ::::::::::::::::::::::::::::::::::::::::::::::: This is added by Eman
		 * K to insert the whole record of patient information.
		 */
		app = ((ApplicationController) getApplicationContext());
//		history = app.getHistory();

		// patient = app.getPatient();
		// pTemp = app.getPTemp();
		mSaveButton = (Button) findViewById(R.id.saveButton);
		// start by ilhamy

		mSaveButton.setTextSize(18);

		// mSaveButton.setTextSize(20);

		// end by ilhamy
		mSaveButton.setOnClickListener(new SaveButtonListener(this));

		if (app.isRecording()) {

/*			if (history.insert(app, history)) {
				Toast.makeText(RecommendationsActivity.this, R.string.save_history_toast, Toast.LENGTH_SHORT).show();
			} else {
				Toast t = Toast.makeText(RecommendationsActivity.this, R.string.Notsave_toast, Toast.LENGTH_SHORT);
				t.getView().setBackgroundColor(Color.RED);
				t.show();

			}*/

			LinearLayout saveButtonLayout = (LinearLayout) this.findViewById(R.id.saveButtonLayout);
			saveButtonLayout.setVisibility(LinearLayout.GONE);
		}

		/*
		 * :::::::::::::::::::::::::::::::::::::::::::::::
		 */
	}

	class NewPatButtonListener implements View.OnClickListener {

		RecommendationsActivity rA;

		public NewPatButtonListener(RecommendationsActivity recommendationsActivity) {
			rA = recommendationsActivity;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {
			// only show popup if patient has not yet been saved	
          /*
			Toast toast = null;
			history.setP_ID(app.getPatient().getP_ID());
			history.setPatientTempreture(app.getPTemp());
			history.setRecommendation(Static.recommendation.toString());
			app.setHistory(history);
			app.setEnd(true);
			if (!app.isMyServiceRunning()) {
				toast = Toast.makeText(RecommendationsActivity.this, R.string.service_is_running, Toast.LENGTH_LONG);
				toast.getView().setBackgroundColor(Color.YELLOW);
				toast.show();
			} else {

				if (!app.isRecording()) {
					new AlertDialog.Builder(rA).setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle("Finishing Diagnosis").setMessage("Do you want to identify the patient?")
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int id) {
									Intent to_patientRecord = new Intent(RecommendationsActivity.this,
											CameraActivity.class);
									startActivity(to_patientRecord);

								}

							}).setNegativeButton("No", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int id) {
									Intent intent = new Intent(RecommendationsActivity.this, HomeActivity.class);
									intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									startActivity(intent);

								}

							}).show();
				} else {
					Intent intent = new Intent(RecommendationsActivity.this, HomeActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					startActivity(intent);

				}
			}*/

		}

	}

	class SaveButtonListener implements View.OnClickListener {

		RecommendationsActivity rA;

		public SaveButtonListener(RecommendationsActivity recommendationsActivity) {
			rA = recommendationsActivity;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View v) {

			app.setEnd(true);
//			Intent to_patientRecord = new Intent(RecommendationsActivity.this, CameraActivity.class);
//			startActivity(to_patientRecord);

		}
	}

	/**
	 * Convert the recommendations structure into a usable format.
	 * 
	 * @param recsStruct
	 *            The Jason structure of all reported outcomes and treatments.
	 */
	private void buildRecommendations(Structure recsStruct) {
		// TODO Refactor this common logic
		// Create a comparator for the recommendations
		RecommendationComparator recCmp = new RecommendationComparator();
		// Outcomes first
		// Has the items to remove duplicates
		Set<Recommendation> outSet = new HashSet<Recommendation>();
		Structure outcomes = (Structure) (recsStruct.getTerm(0));
		// Add each item
		ListTerm outcomeList = (ListTerm) (outcomes.getTerm(0));
		for (Term outcomeTerm : outcomeList) {
			ListTerm outcomeStatus = (ListTerm) (outcomeTerm);
			// String outcomeName = ((StringTerm) outcomeTerm).getString();
			outSet.add(new OutcomeRecommendation(outcomeStatus));
		}
		// Take the hash map and add it all to an ordered set
		Set<Recommendation> outTree = new TreeSet<Recommendation>(recCmp);
		outTree.addAll(outSet);
		for (final Recommendation outcome : outTree) {
			View view = outcome.createView(getApplicationContext());
			listOuts.addView(view);
			// Handle the clicks on the individual recommendation items
			view.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Log.d("RECOMMENDATION_OUTCOME", "Clicked on " + outcome.getLabel());
					onRecommendationClick(outcome);
				}
			});
		}

		// Treatments second
		// TODO Reuse code above
		Set<Recommendation> treatSet = new HashSet<Recommendation>();
		Structure treatments = (Structure) (recsStruct.getTerm(1));
		ListTerm treatmentList = (ListTerm) (treatments.getTerm(0));
		for (Term treatmentTerm : treatmentList) {
			Structure treatmentStruct = (Structure) treatmentTerm;
			treatSet.add(RecommendationFactory.createTreatmentRecommendation(treatmentStruct));
		}
		Set<Recommendation> treatTree = new TreeSet<Recommendation>(recCmp);
		treatTree.addAll(treatSet);
		for (final Recommendation treatment : treatTree) {
			View view = treatment.createView(getApplicationContext());
			listTreats.addView(view);
			// Handle the clicks on the individual recommendation items
			view.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					Log.d("RECOMMENDATION_TREATMENT", "Clicked on " + treatment.getLabel());
					onRecommendationClick(treatment);
				}
			});
		}
	}

	/**
	 * Handle the user clicking on a recommendation item from the list. If there
	 * is an associated image to display, load it up.
	 * 
	 * @param rec
	 *            The recommendation that was clicked.
	 */
	protected void onRecommendationClick(Recommendation rec) {
		// If the recommendation has extra details to display
		if (rec.getDetailsFilename() != null) {
			// Switch to the details image view
			Intent intent = new Intent(getApplicationContext(), TreatmentDetailsActivity.class);
			intent.putExtra("details_filename", rec.getDetailsFilename());
			startActivity(intent);
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
		}
	}

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
//			Intent exit = new Intent(this, HomeActivity.class);
//			exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//			exit.putExtra("EXIT", true);
//			startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}
}
