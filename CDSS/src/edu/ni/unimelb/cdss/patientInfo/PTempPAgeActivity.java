package edu.ni.unimelb.cdss.patientInfo;

import jaca.android.JaCaService;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import edu.ni.unimelb.cdss.R;
import edu.ni.unimelb.cdss.control.ApplicationController;
import edu.ni.unimelb.cdss.diagnosis.listener.ActivityListener;
import edu.ni.unimelb.cdss.diagnosis.stc.Static;
import edu.ni.unimelb.cdss.numberPicker.NumberPicker;

/**
 * This activity to get patient temperature manual or via external device.
 * 
 * @author Team A
 * @version 0.3
 */
public class PTempPAgeActivity extends Activity implements ActivityListener {
	/**
	 * Two buttons
	 */
	private Button mNextButtonTempAge;
	private ImageButton mDateOfBirthImageButton;
	private TextView mDateOfBirthText;
	private TextView mAgeText;
	private EditText mAgeYears;
	private TextView mDateOfbirthEditText;
	private EditText mAgeMonths;
	private TextView mAgeTextYear;
	private TextView mAgeTextMonths;
	ImageButton mHomeButton;
	ImageButton mBackbutton;
	/**
	 * To get the date and the current date from Calendar object
	 */
	private int birth_year, birth_month, birth_day, age;
	private int mYear, mMonth, mDay;
	static final int DATE_DIALOG_ID = 0;

	// start by ilhamy
	ProgressDialog progressDialog;
	protected boolean nextPressedFlag;
	// end by ilhamy

	private NumberPicker mNumPicker;

	private ApplicationController app;

	public PTempPAgeActivity() {
		// Assign current Date
		final Calendar c = Calendar.getInstance();
		mYear = c.get(Calendar.YEAR);
		mMonth = c.get(Calendar.MONTH);
		mDay = c.get(Calendar.DAY_OF_MONTH);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.p_temp_p_age_view);
		app = ((ApplicationController) getApplicationContext());
		mNextButtonTempAge = (Button) findViewById(R.id.btn_temp_age);
		mNextButtonTempAge.setOnClickListener(new NextButtonListener(this));
		nextPressedFlag = false;

		mNumPicker = (NumberPicker) findViewById(R.id.Picker1);

		mHomeButton = (ImageButton) findViewById(R.id.homeButtonTemp);
		mHomeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			// On click function
			public void onClick(View view) {
				// Create the intent to start another activity
				
			}
		});

		mBackbutton = (ImageButton) findViewById(R.id.backButtonTemp);
		mBackbutton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				finish();
			}
		});

		mAgeYears = (EditText) findViewById(R.id.patientAge_ptemp_page);
		mAgeYears.setText("0");
		// Highlight the number when we get focus
		mAgeYears.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					((EditText) v).selectAll();
				}
			}
		});

		mAgeMonths = (EditText) findViewById(R.id.editTextAgeMonths_Ptemp);
		mAgeMonths.setText("0");
		// Highlight the number when we get focus
		mAgeMonths.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					((EditText) v).selectAll();
				}
			}
		});

		mAgeMonths.setFilters(new InputFilter[] { new InputFilterMinMax("0", "11") });
		mAgeTextMonths = (TextView) findViewById(R.id.textView_Ptempmon);
		mAgeTextYear = (TextView) findViewById(R.id.textView_ptempyears);
		mAgeText = (TextView) findViewById(R.id.age_text_ptemp_page);
		mDateOfBirthText = (TextView) findViewById(R.id.date_text_ptemp);
		mDateOfBirthImageButton = (ImageButton) findViewById(R.id.date_of_birth_button_ptemp_page);
		mDateOfbirthEditText = (TextView) findViewById(R.id.birth_date_ptempPAge);
		if (app.isRecording()) {
			mAgeTextYear.setVisibility(View.GONE);
			mAgeTextMonths.setVisibility(View.GONE);
			mAgeMonths.setVisibility(View.GONE);
			mDateOfBirthImageButton.setVisibility(View.GONE);
			mDateOfbirthEditText.setVisibility(View.GONE);
			mDateOfBirthText.setVisibility(View.GONE);
			mAgeYears.setVisibility(View.GONE);
			mAgeText.setVisibility(View.GONE);
			mNumPicker.setNextFocusDownId(R.id.btn_temp_age);
			//String[] token = app.getPatient().getAge().split(" ");

			//app.setpAge_years(token[0]);
			//app.setpAge_months(token[2]);
			//app.setpDateOfBirth(app.getPatient().getDate_of_birth());
		} else {
			mNumPicker.setNextFocusDownId(R.id.date_of_birth_button_ptemp_page);
			mNumPicker.getValueText().setNextFocusDownId(R.id.patientAge_ptemp_page);

			mDateOfBirthImageButton.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
					// Show the DatePickerDialog
					showDialog(DATE_DIALOG_ID);
				}
			});

		}

		mNumPicker.setValue(37.00);

	}

	/**
	 * Register DatePickerDialog listener
	 */
	private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
		// the callback received when the user "sets" the Date in the
		// DatePickerDialog
		public void onDateSet(DatePicker view, int yearSelected, int monthOfYear, int dayOfMonth) {
			birth_year = yearSelected;
			birth_month = monthOfYear + 1;
			birth_day = dayOfMonth;
			int currenMonth = mMonth + 1;
			// Set the Selected Date in Select date EditText
			if (mYear < birth_year || (mYear == birth_year && currenMonth < birth_month)) {
				Toast.makeText(PTempPAgeActivity.this, "The Data Of birth is invalid", Toast.LENGTH_SHORT).show();
			} else {

				mDateOfbirthEditText.setText(birth_day + " - " + birth_month + " - " + birth_year);
				age = 0;
				int day = 0;
				age = mYear - birth_year;
				int months = currenMonth - birth_month;
				// if month difference is in negative then reduce years by one
				// and
				// calculate the number of months.
				if (months < 0) {
					age--;
					months = 12 - birth_month + currenMonth;

					if (mDay < birth_day)
						months--;

				} else if (months == 0 && mDay < birth_day) {
					age--;
					months = 11;
				}
				day = mDay - birth_day;

				if (mDay == birth_day) {
					day = 0;
					if (months == 12) {
						age++;
						months = 0;
					}
				}
				if (age < 0)
					age = 0;
				mAgeYears.setText(String.valueOf(age));

				mAgeMonths.setText(String.valueOf(months));
			}

		}
	};

	/*
	 * @author Team A
	 * 
	 * @see android.app.Activity#onCreateDialog(int) Method automatically gets
	 * Called when you call showDialog() method
	 */
	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case DATE_DIALOG_ID:
			// create a new DatePickerDialog with values you want to show
			return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
		}
		return null;
	}

	class NextButtonListener implements View.OnClickListener {
		PTempPAgeActivity Temp;

		public NextButtonListener(PTempPAgeActivity pTempPAgeActivity) {
			Temp = pTempPAgeActivity;
		}

		@Override
		public void onClick(View v) {

			if (!app.isRecording()
					&& (mDateOfbirthEditText.getText().length() == 0 && mAgeYears.getText().length() == 0)) {
				mAgeYears.setError("Please enter Patient age OR date of birth");
			}

			else {
				app.setPTemp(mNumPicker.getValue());

				app.setpDateOfBirth(mDateOfbirthEditText.getText().toString());
				app.setpAge_years(mAgeYears.getText().toString());
				app.setpAge_months(mAgeMonths.getText().toString());
				if (Integer.valueOf(mAgeYears.getText().toString()) > 5) {
					new AlertDialog.Builder(Temp)
							.setIcon(android.R.drawable.ic_dialog_alert)
							.setTitle("Patient Age")
							.setMessage(
									"Sorry, this application can only diagnose children up to 5 years of age. Do you want to skip the diagnosis process?")
							.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog, int id) {
									//Intent intent = new Intent(PTempPAgeActivity.this, HomeActivity.class);
									//intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
									//startActivity(intent);

								}

							}).setNegativeButton("No", null).show();
				} else {

					// start by ilhamy
					if (!nextPressedFlag) {
						nextPressedFlag = true;
						progressDialog = ProgressDialog.show(v.getContext(), "", "Please wait...");
						Thread thread = new Thread() {
							public void run() {
								try {
									Thread.sleep(100);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
								try {
									Intent service = new Intent(getApplicationContext(), JaCaService.class);
									service.putExtra(JaCaService.MAS2J, R.string.mas2j);
									getApplicationContext().stopService(service);
									getApplicationContext().startService(service);
								} catch (RuntimeException e) {
								}

								Log.d("Activity", "MAS2J started!");
							}
						};
						thread.start();
					}
					// end by ilhamy

				}
			}
		}
	}

	// start by ilhamy
	@Override
	public void next(String activity) {
		try {
			progressDialog.dismiss();
			startActivity(new Intent(this, Class.forName(activity)));
			overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
			nextPressedFlag = false;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		Static.currentActivity = this;
	}

	// end by ilhamy

	private int group1Id = 1;

	int exit = 1;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		menu.add(group1Id, exit, exit, "").setTitle("Exit");

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {

		case 1:
			//Intent exit = new Intent(this, HomeActivity.class);
			//exit.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			//exit.putExtra("EXIT", true);
			//startActivity(exit);
			return true;

		default:
			break;

		}
		return super.onOptionsItemSelected(item);
	}

}
