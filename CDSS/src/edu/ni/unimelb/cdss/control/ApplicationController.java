package edu.ni.unimelb.cdss.control;

import android.util.Log;
//import edu.ni.unimelb.cdss.diagnosis.DiagnosisHistory;
//import edu.ni.unimelb.cdss.patientInfo.PatientInfo;

/**
 * This class is to keep "PatientInfo" and "Temperature" objects to be accessed
 * (set and get ) by any other Component in the application. "PatientInfo" and
 * "Temperature" objects will be setting by "PatientInformationActivity"
 * "PatientInfo" and "Temperature" objects will be getting or using by
 * "DangerSingnsActivity"
 * 
 * @version 0.3
 * @author Team A
 * 
 */

public class ApplicationController extends android.app.Application {

	//private PatientInfo patient;
//	private DiagnosisHistory history;
	private String pTemp;
	private boolean recording;
	private boolean end;
	private String pAge_years;
    private String pAge_months;
	private String pDateOfBirth;

	public ApplicationController() {

	}

	/**
	 * Constructor function for patient information
	 */
	public void initPatientInfo() {
		//patient = new PatientInfo();
	}

	/**
	 * Constructor function for patient Tempreture
	 */
	public void initPatientPreInfo() {
		pTemp = "";
		pAge_years = "";
		pDateOfBirth = "";

	}

	public void initPatientHisroty() {
//		history = new DiagnosisHistory();

	}

	/*
	 * @see android.app.Application#onCreate()
	 */
	@Override
	public void onCreate() {
		super.onCreate();
		Log.i("CDSS", " APTCTRL CREATED!");
	}

	/**
	 * This function could be used by any Component in the application that want
	 * to get the current patient Information.
	 * 
	 * @return patient : which contains Patient Information such as, name age
	 *         ..etc
	 */
	/*public PatientInfo getPatient() {
		return this.patient;
	}*/

	/**
	 * @param p
	 *            PatientInfo
	 */
	/*public void setPatient(PatientInfo p) {
		this.patient = p;
	}*/

	/*
	 * This function could be used by any Component in the application that want
	 * to get the current patient Temperature.
	 * 
	 * @return pTemp: which is patient's temperature
	 */
	public String getPTemp() {
		return pTemp;
	}

	/**
	 * This function will be called by "TemperatureActivity" to set the current
	 * patient temperature.
	 * 
	 * @param ptemp
	 *            : which is patient's temperature
	 */
	public void setPTemp(String ptemp) {
		this.pTemp = ptemp;
	}

	/**
	 * 
	 * @return true if the patient already record
	 */
	public boolean isRecording() {
		return recording;
	}

	/**
	 * 
	 * @param recording
	 */
	public void setRecording(boolean recording) {
		this.recording = recording;
	}

	/**
	 * 
	 * @return pDateOfBirth
	 */
	public String getpDateOfBirth() {
		return pDateOfBirth;
	}

	/**
	 * 
	 * @param pDateOfBirth
	 */
	public void setpDateOfBirth(String pDateOfBirth) {
		this.pDateOfBirth = pDateOfBirth;
	}

	/**
	 * 
	 * @return pAge_years
	 */
	public String getpAge_years() {
		return pAge_years;
	}

	/**
	 * 
	 * @param pAge_years
	 */
	public void setpAge_years(String pAge_years) {
		this.pAge_years = pAge_years;
	}

	public boolean isEnd() {
		return end;
	}

	public void setEnd(boolean end) {
		this.end = end;
	}

    public String getpAge_months() {
        return pAge_months;
    }

    public void setpAge_months(String pAge_months) {
        this.pAge_months = pAge_months;
    }
}
