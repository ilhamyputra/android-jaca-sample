// Agent non_imci_dengue_fever in project cdss.mas2j
// @Author : Eman Alatawi. 

/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */


epsilon(1.05).// threshold value for ranking 
lambda(5). // maximum number of questions per session
varphi(33).// 25percent lower boundary condition when disease can be ruled out 
theta(67). // 75percent upper  boundary condition when disease can be ruled out 

// ---- Main features ---------------------------

pretest_starting_Odds(69,73).


// ---- Main features ---------------------------
	 			
featureID(kf_exhaustion,1).

feature(dengue_fever,
		kf_exhaustion,
	 	"Is the child exhausted?",
		"",
		"",9).
		
// featureLR(feature_ID ,Serial_number,LR+, LR-, rank).		
featureLR(kf_exhaustion,1.043, 0.264, 3.78).

//--------------Category 2 ---------------------

featureID(kf_Lymph_node_enlargement,2).

feature(dengue_fever,
		kf_Lymph_node_enlargement,
	 	"Does the child have Lymph node enlargement?",
		"",
		"",10).
		
featureLR(kf_Lymph_node_enlargement,2.494 ,0.646 , 2.49).

//----------------------------------------------

featureID(kf_Petechiae,3).


feature(dengue_fever,
		kf_Petechiae,
	 	"Does the child have Petechiae?",
		"",
		"",10).

featureLR(kf_Petechiae,2.380,0.920, 2.38).

//----
featureID(kf_earache,4).

feature(dengue_fever,
		kf_earache,
	 	"Does the child have Earache?",
		"aaaa",
		"",10).

featureLR(kf_earache,2.328,0.902,2.33).

//----- 

featureID(kf_history_of_rash,5).

feature(dengue_fever,
		kf_history_of_rash,
	 	"Does the child have any history of rash?",
		"",
		"",10).

featureLR(kf_history_of_rash,2.267, 0.699, 2.27).

//--------

featureID(kf_history_of_bleeding,6).

feature(dengue_fever,
		kf_history_of_bleeding,
	 	"Does the child have any history of bleeding?","","", 9).

featureLR(kf_history_of_bleeding,2.116,0.900, 2.12).

//----- 

featureID(kf_conjunctival_redness,7).

feature(dengue_fever,
		kf_conjunctival_redness,
	 	"Does the child have any conjunctival redness?","","", 9).

featureLR(kf_conjunctival_redness,2.010,0.619,2.01).

//----- 
featureID(kf_rash,8).
feature(dengue_fever,
		kf_rash,
	 	"Does the child have rash?","","", 9).

featureLR(kf_rash,1.638, 0.529,1.89).


//----- 

featureID(kf_itching,9).
feature(dengue_fever,
		kf_itching,
	 	"Does the child have itching?","","", 9).

featureLR(kf_itching,1.709, 0.846,1.71).

// ----- 
featureID(kf_sore_throat,10).
feature(dengue_fever,
		kf_sore_throat,
	 	"Does the child have sore throat?","","", 9).

featureLR(kf_sore_throat,0.627,1.219,1.60).
	
//-----
featureID(kf_taste_disorder,11).
feature(dengue_fever,
		kf_taste_disorder,
	 	"Does the child have taste disorder?","","", 9).

featureLR(kf_taste_disorder,1.381, 0.629,1.59).

//-----
featureID(kf_dehydration,12).
feature(dengue_fever,
		kf_dehydration,
	 	"Does the child have dehydration?","","", 9).

featureLR(kf_dehydration,1.587, 0.835,1.59).

//-----

featureID(kf_dyspnea,13).
feature(dengue_fever,
		kf_dyspnea,
	 	"Does the child have dyspnea?","","", 9).

featureLR(kf_dyspnea,1.511, 0.946,1.51).


//--------------Category 3 ---------------------
featureID(kf_hepatomegaly,14).
feature(dengue_fever,
		kf_hepatomegaly,
	 	"Does the child have hepatomegaly?","","", 9).

featureLR(kf_hepatomegaly,1.481,0.965,1.48).

//-----
featureID(kf_diarrhea,15).
feature(dengue_fever,
		kf_diarrhea,
	 	"Does the child have diarrhea?","","", 9).

featureLR(kf_diarrhea,1.469,0.846,1.47).


//-----
featureID(kf_abdominal_pain,16).
feature(dengue_fever,
		kf_abdominal_pain,
	 	"Does the child have abdominal pain?","","", 9).

featureLR(kf_abdominal_pain,0.717,1.209,1.40).

//-----
//-----

featureID(kf_Retro_orbital_pain,17).
feature(dengue_fever,
		kf_Retro_orbital_pain,
	 	"Does the child have retro-orbital pain?","","",9).

featureLR(kf_Retro_orbital_pain,1.178,0.730,1.37).

//-----
featureID(kf_Vomiting,18).
feature(dengue_fever,
		kf_Vomiting,
	 	"Is the child vomiting?","","",9).

featureLR(kf_Vomiting,1.369,0.888,1.37).

//-----
featureID(kf_Pharyngeal_erythema,19).
feature(dengue_fever,
		kf_Pharyngeal_erythema,
	 	"Does the child have pharyngeal erythema?","","",9).

featureLR(kf_Pharyngeal_erythema,0.756,1.099,1.32).


//-----
featureID(kf_Lumbar_pain,20).
feature(dengue_fever,
		kf_Lumbar_pain,
	 	"Does the child have lumbar pain?","","",9).

featureLR(kf_Lumbar_pain,1.058,0.756,1.32).

//-----
featureID(kf_Nausea,21).
feature(dengue_fever,
		kf_Nausea,
	 	"Does the child have nausea?","","",9).

featureLR(kf_Nausea,1.190,0.769,1.30).
	
//-----
featureID(kf_Hoarseness,22).
feature(dengue_fever,
		kf_Hoarseness,
	 	"Does the child have hoarseness?","","",9).

featureLR(kf_Hoarseness,1.293,0.959,1.29).
	
//-----
featureID(kf_Pallor,23).
feature(dengue_fever,
		kf_Pallor,
	 	"Does the child have pallor?","","",9).

featureLR(kf_Pallor,0.776,1.058,1.29).
//-----
featureID(kf_Chills,24).
feature(dengue_fever,
		kf_Chills,	
	 	"Does the child have chills?","","",9).

featureLR(kf_Chills,0.831,1.229,1.23).

//-----
featureID(kf_Anorexia,25).
feature(dengue_fever,
		kf_Anorexia,
	 	"Does the child have anorexia?","","",9).

featureLR(kf_Anorexia,1.058,0.823,1.22).
	
//-----
featureID(kf_Photophobia,26).
feature(dengue_fever,
		kf_Photophobia,
	 	"Does the child have photophobia?","","",9).

featureLR(kf_Photophobia,1.149,0.863,1.16).
	
//-----
featureID(kf_Nasal_congestion,27).
feature(dengue_fever,
		kf_Nasal_congestion,
	 	"Does the child have nasal congestion?","","",9).

featureLR(kf_Nasal_congestion,0.871,1.039,1.15).

//-----
featureID(kf_Myalgia,28).
feature(dengue_fever,
		kf_Myalgia,
	 	"Does the child have myalgia?","","",9).

featureLR(kf_Myalgia,1.011,0.882,1.13).	


//-----
featureID(kf_Arthralgia,29).
feature(dengue_fever,
		kf_Arthralgia,
	 	"Does the child have arthralgia?","","",9).

featureLR(kf_Arthralgia,1.083,0.882,1.13).

//-----
featureID(kf_Headache,30).
feature(dengue_fever,
		kf_Headache,
	 	"Does the child have headache?","","",9).

featureLR(kf_Headache,0.992,1.058,1.06).


//-----
featureID(kf_Splenomegaly,31).
feature(dengue_fever,
		kf_Splenomegaly,
	 	"Does the child have splenomegaly?","","",9).

featureLR(kf_Splenomegaly,1.058,0.996,1.06).



//-----
featureID(kf_Dizziness,32).
feature(dengue_fever,
		kf_Dizziness,
	 	"Does the child have splenomegaly?","","",9).

featureLR(kf_Dizziness,1.027,0.977,1.03).



//-----
featureID(kf_Cough,33).
feature(dengue_fever,
		kf_Cough,
	 	"Does the child have cough?","","",9).

featureLR(kf_Cough,1.017,0.990,1.02).


//-----
featureID(kf_Coryza,34).
feature(dengue_fever,
		kf_Coryza,
	 	"Does the child have coryza?","","",9).

featureLR(kf_Coryza,1.008,0.997,1.01).

//----------------------------------------------------------------------------

 
outcomeSpecification(dengue_fever_very_likely,"dengue_fever_very_likely") 
:-  keysymptom(dengue_fever, yes)
 & posttestPropability (N)
 & N >= 90
 & post.


outcomeSpecification(dengue_fever_very_unlikely, "  dengue_fever_very_unlikely") 
:-  keysymptom(dengue_fever, yes)
 & posttestPropability (N)
 & N <= 10
 & post.

outcomeSpecification(dengue_fever_unlikely, "dengue_fever_unlikely") 
:-  keysymptom(dengue_fever, yes)
 & posttestPropability (N)
 & (N > 10 & N <=33)
 & post.

outcomeSpecification(dengue_fever_uncertain , "dengue_fever_uncertain") 
:-  keysymptom(dengue_fever, yes)
 & posttestPropability (N)
 & (N >= 34 & N <= 66)
 & post.

outcomeSpecification(dengue_fever_likely , "dengue_fever_likely") 
:-  keysymptom(dengue_fever, yes)
 & posttestPropability (N)
 & (N >= 67 & N < 90)
 & post.

outcome(
	["Dengue fever is very likely (Probability is geater than 90%)","HIGH"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", high, "")
			
		])
	):- outcomeSpecification(dengue_fever_very_likely, _).

outcome(
	["Dengue fever is very unlikely (Probability is less than  10%)","LOW"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", high, "")
		])
	):- outcomeSpecification(dengue_fever_very_unlikely, _).


outcome(
	["Dengue fever is unlikely (Probability is between 10% and 33%)","LOW"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", high, "")
		])
	):- outcomeSpecification(dengue_fever_unlikely, _).
	

outcome(
	["Dengue fever is Uncertain (Probability is between 34% and 66%)","LOW"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", high, "")
		])
	):- outcomeSpecification(dengue_fever_uncertain, _).
	
outcome(
	["Dengue fever is Likely (Probability is between 67% and 90%)","MEDIUM"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", high, "")
		])
	):- outcomeSpecification(dengue_fever_likely, _).	
	
/* Initial goals */

!start.


/* Plans */

+!start <-
	// Register key feature data.
	//for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
	.send(feature,tell,question(dengue_fever, kf_loc, "Is the Child in Brazil or has been there recently?", "", "", 8));
//	}

.

+answer(kf_loc,na): keysymptom(dengue_fever, yes) <-	
    +questionsCounter(1);
	?pretest_starting_Odds(A,B);
	C= A/B;
	+posttestOdd(C); // Add post test odds to belife's base
	D = C/(C+1);
	F =D *100;	
	+posttestPropability(F);// Add post test odds to belife's base*/
	+done(false);
    !startAsking.
	
	
+answer(kf_loc,yes): keysymptom(dengue_fever, yes) <-	
    +questionsCounter(1);
	?pretest_starting_Odds(A,B);
	C= A/B;
	+posttestOdd(C); // Add post test odds to belife's base
	D = C/(C+1);
	F =D *100;	
	+posttestPropability(F);// Add post test odds to belife's base*/
	+done(false);
    !startAsking.

	
+answer(kf_loc,no): keysymptom(dengue_fever, yes) <-	
	+posttestPropability(0);
	+done(true);// Add post test odds to belife's base*/
	+keepAsking(no);
	!declareOutcome.



+!startAsking<-
?featureID(KF,1);
?featureLR(KF,LR1, LR2, R);
?feature(dengue_fever, KF, QUESTION, HELP, IMAGE, PRIORITY);
.send(feature,tell,question(dengue_fever, KF, QUESTION, HELP, IMAGE, PRIORITY));
+keepAsking(yes);
.

+answer(FEATURE,ANS): keysymptom(dengue_fever, yes) <-
!calculatePostTestProbability(FEATURE, ANS).


/* Plan 2: calculatePostTestProbability
			Based on each received answer for specifc required feature,
			the agent will excute the paln [ calculatePostTestProbability ] to 
			calculate the post test propbability.
*/

+!calculatePostTestProbability(FEATURE, ANS) <-

?featureLR(FEATURE,PositiveLR,NegativeLR,R);
?featureID(FEATURE,N);
?posttestOdd(Odd);

/*
When feature is positively presented, the agent will use
Pristive LR 
*/

if(ANS == yes)
{
NewOdd1 = Odd*PositiveLR;
}

/*
When feature is negatively abcent, the agent will use
Negative LR 
*/

if(ANS == no)
{
NewOdd1 = Odd*NegativeLR;
}

if(ANS == na)
{
NewOdd1 = Odd;
}

/* Agent updates the post test Odd accordingly */

-+posttestOdd(NewOdd1);

/* Agent Converts the post test odd to propability
and then update the post test probability 
*/

D1 = NewOdd1/(NewOdd1+1);
F1 =D1 *100;
-+posttestPropability(F1);


/* The agent will look ahead to determine next question */ 

!lookAheadAndPrepareNextQuestion(N).

//----------------------------------------------------------------------------

/* Plan 2:lookAheadAndPrepareNextQuestion (N)
	N = serial number of previous question 
*/

+!lookAheadAndPrepareNextQuestion(N): keepAsking(yes) <- 
?featureID(FE, N+1);
?featureLR(FE,PositiveLR3,NegativeLR3,R3);
?feature(dengue_fever, FE, QUESTION, HELP, IMAGE, PRIORITY);

?epsilon(E);
?lambda(L);

if(R3 > E)
{
?questionsCounter(C);
?lambda(L);
if (C < L)
{
	.send(feature,tell,question(dengue_fever, FE, QUESTION, HELP, IMAGE, PRIORITY));
	XX = C + 1; 
	-+questionsCounter(XX);	
	-+done(false);
	-+keepAsking(yes);

}

if ((C > L)|(C==L))
{
?posttestPropability(PP);
?varphi(V);
?theta(T);
if((PP > T)|(PP < V))
{
-+keepAsking(no);
-+done(true);
!declareOutcome;
}
else
{

!reserQuestionsCounter;
.send(feature,tell,question(dengue_fever, FE, QUESTION, HELP, IMAGE, PRIORITY));
?questionsCounter(CC);
XXX= CC + 1; 
-+questionsCounter(XXX);	
-+keepAsking(yes);

}

}

}
else
{
-+keepAsking(no);
-+done(true);
!declareOutcome;
}

.

+!reserQuestionsCounter:keepAsking(yes)<-
-+questionsCounter(0);
-+done(false).


+!declareOutcome: keepAsking(no) <-
+post.
	

+!checkOutcome : not response(yes) & keysymptom(dengue_fever, yes) 
		<-
		.perceive;
		
		if(keysymptom(dengue_fever, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(dengue_fever, yes));
			}else{
						
			.send(feature, tell, response(dengue_fever, no));


			}		
		}
.

		

