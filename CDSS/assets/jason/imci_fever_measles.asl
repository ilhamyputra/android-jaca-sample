
/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */

// ----main features ---------------------------
	

feature(fever_measles,
		kf_measles_now,
	 	"Does the child currently have measles?",
	 	"Check for a high fever, which begins about 10 to 12 days after exposure to the virus. A runny nose, a cough, red and watery eyes, and small white spots inside the cheeks can develop in the initial stage. Check for a rash on the face and upper neck. Over about three days, the rash spreads, eventually reaching the hands and feet. The rash lasts for five to six days, and then fades.",
	 	"measles_help", 10).

feature(fever_measles,
		kf_measles_within_3_months,
	 	"Did the child have measles within the past 3 months?",
	 	"A child with fever and a history of measles within the last 3 months may have an infection, such as an eye infection, due to complications of measles. In areas with a high measles prevalence, mothers are often able to recognize the disease.",
	 	"measles_help", 10).

feature(fever_measles,
		kf_eye_problems_cloudy_cornea,
	 	"Does the child have a cloudy cornea?",
	 	"When clouding of the cornea is present, there is a hazy area in the cornea. Look carefully at the cornea for clouding. The cornea may appear clouded or hazy, such as how a glass of water looks when you add a small amount of milk. The clouding may occur in one or both eyes.",
	 	"cloudy_cornea_help", 10).

feature(fever_measles,
		kf_mouth_ulcers,
	 	"Does the patient have mouth ulcers?",
	 	"Look inside the child's mouth for mouth ulcers. Ulcers are painful open sores on the inside of the mouth and lips or the tongue. They may be red or have white coating on them.",
	 	"", 10).
	 	
feature(fever_measles,
		kf_deep_or_extensive_mouth_ulcers,
	 	"Does the patient have deep or excessive mouth ulcers?",
	 	"Look inside the child's mouth for mouth ulcers. Check if deep or large ( > 10mm )",
	 	"", 10, kf_mouth_ulcers, yes).	 	

feature(fever_measles,
		kf_eye_problems_draining_pus,
	 	"Is pus draining from the child's eye?",
	 	"Pus draining from the eye is a sign of conjunctivitis. If you do not see pus draining from the eye, look for pus on the conjunctiva or on the eyelids. Often the pus forms a crust when the child is sleeping and seals the eye shut. It can be gently opened with clean hands. Wash your hands after examining the eye of any child with pus draining from the eye.",
	 	"", 11).


/*FEATURE REQUIRED */
featureRequired(severe_complicated_measles, kf_measles_now).
featureRequired(severe_complicated_measles, kf_measles_within_3_months).
featureRequired(severe_complicated_measles, kf_eye_problems_cloudy_cornea).
featureRequired(severe_complicated_measles, kf_mouth_ulcers).
featureRequired(severe_complicated_measles, kf_deep_or_extensive_mouth_ulcers)
:-answer(kf_mouth_ulcers,yes).
featureRequired(severe_complicated_measles, dangersigns).
featureRequired(measles_with_eye_or_mouth_complications, kf_mouth_ulcers).
featureRequired(measles_with_eye_or_mouth_complications, kf_eye_problems_draining_pus).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.
	
/* INDIVIDUAL OUTCOMES SPECIFICATION */

outcomeSpecification(severe_complicated_measles, "Severe Complicated Measles") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_measles_now,
 					kf_measles_within_3_months
 					])),N)
 & N > 0
 &.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_eye_problems_cloudy_cornea,
 					kf_deep_or_extensive_mouth_ulcers,
 					dangersigns
 					])),M)
 & M > 0
 & keysymptom(fever_measles, yes)
 & outcomeDone(severe_complicated_measles)
.

outcomeSpecification(measles_with_eye_or_mouth_complications, "Measles With Eye Or Mouth Complications") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_measles_now,
 					kf_measles_within_3_months
 					])),N)
 & N > 0
 &.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_eye_problems_draining_pus,
 					kf_mouth_ulcers
 					])),M)
 & M > 0
 //add
 & not outcomeSpecification(severe_complicated_measles, _)
 & not answer(kf_deep_or_extensive_mouth_ulcers, yes)
 & keysymptom(fever_measles, yes)
 & outcomeDone(severe_complicated_measles)
 & outcomeDone(measles_with_eye_or_mouth_complications)
.

outcomeSpecification(measles, "Measles") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_measles_now,
 					kf_measles_within_3_months
 					])),N)
 & N > 0	
 & not outcomeSpecification(measles_with_eye_or_mouth_complications, _)
 & not outcomeSpecification(severe_complicated_measles, _)
 & keysymptom(fever_measles, yes)
 & outcomeDone(severe_complicated_measles)
 & outcomeDone(measles_with_eye_or_mouth_complications)
.

outcome(
	["Severe Complicated Measles","HIGH"],
		treatments([treatment(referral,referral_parameters("Hospital",duration(0,0,0,0,0,0),"", true),"", high, ""),
					treatment(prescribed_supplement,prescribed_supplement_parameters("Vitamin A ","" ,duration(0,0,0,0,0,0)),"", high,"p11_l_diag"),
					treatment(prescribed_drug,prescribed_drug_parameters("Tetracycline eye ointment","" ,duration(0,0,0,0,0,0)),"If clouding of the cornea or pus draining from the eye", high,"p10_br_diag"),
					treatment(prescribed_drug, prescribed_drug_parameters("Antibiotic", "First dose", duration(0,0,0,0,0,0)), "", high, "p8_tr_tabl")
					])
	):- outcomeSpecification(severe_complicated_measles,_).
	
outcome(
	["Measles With Eye Or Mouth Complications","MEDIUM"],
		treatments([treatment(prescribed_supplement,prescribed_supplement_parameters("Vitamin A ","" ,duration(0,0,0,0,0,0)),"", medium,"p11_l_diag"),
					treatment(prescribed_drug,prescribed_drug_parameters("Tetracycline eye ointment","" ,duration(0,0,0,0,0,0)),"If pus draining from the eye", medium,"p10_br_diag"),
					treatment(prescribed_drug,prescribed_drug_parameters("Gentian Violet","" ,duration(0,0,0,0,0,0)),"If mouth ulcers", medium,"p10_tr_diag"),
					treatment(follow_up,follow_up_parameters(duration(0,0,2,0,0,0)),"", medium,"p17_br_diag")
					])
	):- outcomeSpecification(measles_with_eye_or_mouth_complications,_).

outcome(
	["Measles","LOW"],
		treatments([treatment(prescribed_supplement,prescribed_supplement_parameters("Vitamin A ","" ,duration(0,0,0,0,0,0)),"", low,"p11_l_diag")])
	):- outcomeSpecification(measles,_).	
	
	
	
/* Initial goals */

!start.


/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
	
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER)) {		
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY, DEPENDENCY, ANSWER));
	}
.

+!checkOutcome : not response(yes) & keysymptom(fever_measles, yes)
		<-
		.perceive;
		if(keysymptom(fever_measles, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(fever_measles, yes));
			}else{
				.send(feature, tell, response(fever_measles, no));
			}		
		}
.
