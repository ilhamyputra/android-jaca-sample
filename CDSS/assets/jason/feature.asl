
/* Initial beliefs and rules */

countResponse(0).

/* Initial goals: */

!initialise.

/* Plan definitions: */

/* - Internal plans: */

// Action:	Initialisation and setup of the agent.
+!initialise <-
	// Tell the DisplayEnvironment that the agents are ready.
	.print("Initialised.");
	agentsIntialised;.

+answer(DEPENDENCY, ANSWER) : true
	 		<-
	 			for(answer(DEPENDENCY, ANSWER) & question(AGT, FTR, QTN, HLP, IMG, PRIORITY, DEPENDENCY, ANSWER)){
	 				+question(AGT, FTR, QTN, HLP, IMG, PRIORITY-1);
	 			 	.abolish(question(AGT, FTR, QTN, HLP, IMG, PRIORITY, DEPENDENCY, ANSWER));
	 			 }
.

+ask_feature : true
	 		<- 	.abolish(ask_feature);  
	 			.broadcast(achieve,checkOutcome);
	 			while(.count(keysymptom(_, yes),P) & .count((response(AGNT,_) & keysymptom(AGNT, yes)),O) & not(P = O)){}
	 			.abolish(response(_,no));
	 			for(response(AGENTS,yes)){
	 				.abolish(question(AGENTS, _, _, _, _, _));
	 				.abolish(question(AGENTS, _, _, _, _, _, _, _));
	 			}	 	
	 			 .findall(question(PRIORITY, AGENT, FEATURE, QUESTION, HELP, IMAGE),
	 			 		(keysymptom(AGENT, yes)
	 			 		& question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)
	 			 		& not answer(FEATURE, ANSWER)
	 			 		& not response(AGENT, yes)
	 			 		),
	 			 		FEATURE_LIST
	 			 );
	 			 if(.length(FEATURE_LIST) > 0){
			   	 .sort(FEATURE_LIST, SORTED_LIST);
			   		.nth(0,SORTED_LIST, question(PRIOR, AGT, FEAT, QUEST, HELPS, IMGE));
			   		.abolish(question(_, FEAT, QUEST,_,_,_));
			   		if(.count(response(_,yes),N) & countResponse(M) & N > M){
	 					-+countResponse(M+(N-M));
	 					.print(QUEST);
	 					.send(user_interaction,achieve,question(PRIOR, AGT, FEAT, QUEST, HELPS, IMGE, yes));
	 				}	
			   		else{
			   			.print(QUEST);
			   			.send(user_interaction,achieve,question(PRIOR, AGT, FEAT, QUEST, HELPS, IMGE, no));
			   		}
	 		   }
	 		   else{
	 		   		.send(outcome, tell, requestOutcome);	
	 		   }			   
.
