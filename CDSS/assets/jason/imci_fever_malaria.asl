
/* Initial beliefs and rules */


/* INDIVIDUAL KEY FEATURES SPECIFICATION */

// ---- Main features ---------------------------
	 	
feature(fever_malaria,
		kf_high_malaria_risk,
	 	"Is the child in a high malaria risk environment?",
	 	"Check if the area has breeding grounds for the mosquito: shallow collections of fresh water, such as puddles, rice fields, and hoof prints.",
	 	"", 9).
	 	
feature(fever_malaria,
		kf_stiff_neck,
	 	"Does the child have a stiff neck?",
	 	"If the child is moving and bending his neck, he does not have a stiff neck. Look to see if the child can bend his neck when he looks down at his umbilicus or toes. If you still have not seen the child bend his neck himself, ask the mother to help you lie the child on his back. Lean over the child, gently support his back and shoulders with one hand. With the other hand, hold his head. Then carefully bend the head forward toward his chest. If the neck bends easily, the child does not have stiff neck. If the neck feels stiff and there is resistance to bending, the child has a stiff neck. Often a child with a stiff neck will cry when you try to bend the neck.",
	 	"stiff_neck_help", 10).
	 	
feature(fever_malaria,
		kf_runny_nose,
	 	"Does the child have a runny nose?",
	 	"Ask the mother if the child has a acute or chronic runny nose, or if it is accompanied with the common cold.",
	 	"", 11).

feature(fever_malaria,
		kf_measles_now,
	 	"Does the child currently have measles?",
	 	"Check for a high fever, which begins about 10 to 12 days after exposure to the virus. A runny nose, a cough, red and watery eyes, and small white spots inside the cheeks can develop in the initial stage. Check for a rash on the face and upper neck. Over about three days, the rash spreads, eventually reaching the hands and feet. The rash lasts for five to six days, and then fades.",
	 	"measles_help", 11).
	 	
feature(fever_malaria,
		kf_cause_of_fever,
	 	"Does the child have any cause of fever?",
	 	"Check if the child has any cause of fever OTHER than malaria (for example, an ear infection).",
	 	"", 11).


/*FEATURE REQUIRED */
featureRequired(very_severe_febrile_disease, kf_stiff_neck).
featureRequired(very_severe_febrile_disease, dangersigns).
featureRequired(malaria, kf_high_malaria_risk).
featureRequired(malaria, kf_runny_nose).
featureRequired(malaria, kf_cause_of_fever).
featureRequired(malaria, kf_measles_now).
outcomeDone(DIAGNOSIS)
:- .count(featureRequired(DIAGNOSIS,_), M)
 & .count((featureRequired(DIAGNOSIS,FEATURE)
   		  &answer(FEATURE,_)),N)  
 & M = N 	
.

/* INDIVIDUAL OUTCOMES SPECIFICATION */

// VERY SEVERE FEBRILE DISEASE
// - Any general danger sign
// OR
// - Stiff neck
outcomeSpecification(very_severe_febrile_disease, "Very Severe Febrile Disease") 
:-.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_stiff_neck,
 					dangersigns
 					])),N)
 & N > 0
 & keysymptom(fever_malaria, yes)
 & outcomeDone(very_severe_febrile_disease)
.

// MALARIA
//   - High malaria risk
// - AND
//   - Fever
// OR
//     - Low/normal malaria risk
//   - AND
//     - No runny nose
//   - AND
//     - No measles
//   - AND
//     - No other cause of fever (generalised rash, cough, red eyes)
outcomeSpecification(malaria, "Malaria") 
:- (answer(kf_high_malaria_risk, yes)
 | (answer(kf_high_malaria_risk, no)
 & answer(kf_runny_nose, no)
 & answer(kf_cause_of_fever, no)
 & answer(kf_measles_now, no)))
 //add
 & not outcomeSpecification(very_severe_febrile_disease, _) 
 & keysymptom(fever_malaria, yes)
 & outcomeDone(very_severe_febrile_disease)
 & outcomeDone(malaria)
.

// FEVER MALARIA UNLIKELY
// - Low/normal malaria risk
// AND
// - No runny nose
// AND
// - No measles
// AND
// - No other cause of fever (generalised rash, cough, red eyes)
outcomeSpecification(fever_malaria_unlikely, "Fever (Malaria Unlikely)") 
:- answer(kf_high_malaria_risk, no)
 &.count((answer(FEATURE, yes) 
		& .member(FEATURE,
		 	       [kf_runny_nose,
 					kf_cause_of_fever,
 					kf_measles_now
 					])),N)
 & N > 0
 //add
 & not outcomeSpecification(very_severe_febrile_disease, _) 
 & not outcomeSpecification(malaria, _) 
 & keysymptom(fever_malaria, yes)
 & outcomeDone(very_severe_febrile_disease)
 & outcomeDone(malaria)
.

outcome(
	["Very Severe Febrile Disease","HIGH"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "", true), "", high, ""),
			treatment(prescribed_drug, prescribed_drug_parameters("Quinine", "First dose", duration(0,0,0,0,0,0)), "If severe malaria present", high,"p12_br_diag"),
			treatment(prescribed_drug, prescribed_drug_parameters("Antibiotic", "First dose", duration(0,0,0,0,0,0)), "", high, "p8_tr_tabl"),
			treatment(prescribed_drug, prescribed_drug_parameters("Paracetamol", "One dose", duration(0,0,0,0,0,0)), "If high fever present", high, ""),
			treatment(advice, advice_parameters("Treat the child to prevent low blood sugar"), "", high,"p13_diag")
		])
	):- outcomeSpecification(very_severe_febrile_disease, _).
	
outcome(
	["Malaria","MEDIUM"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", medium, ""),
			treatment(prescribed_drug, prescribed_drug_parameters("Oral co-artemether or other recommended antimalarial", "", duration(0,0,0,0,0,0)), "", medium,"p9_br_tabl"),
			treatment(prescribed_drug, prescribed_drug_parameters("Paracetamol","One dose", duration(0,0,0,0,0,0)), "If high fever presents(38.5 or above)", medium, ""),
			treatment(advice, advice_parameters("Advise mother when to return immediately"), "", medium,"p23_br_tabl"),
			treatment(follow_up, follow_up_parameters(duration(0,0,2,0,0,0)), "if fever persists", medium,"p17_l_diag")
		])
	):- outcomeSpecification(malaria, _).
	
outcome(
	["Fever (Malaria Unlikely)","LOW"],
		treatments([
			treatment(referral, referral_parameters("Hospital", duration(0,0,0,0,0,0), "assessment", false), "If fever is present every day for more than 7 days", low, ""),
			treatment(prescribed_drug, prescribed_drug_parameters("Paracetamol", "One dose", duration(0,0,0,0,0,0)), "If high fever presents(38.5?C or above)", low, ""),
			treatment(advice, advice_parameters("Advise mother when to return immediately"), "", low,"p23_br_tabl"),
			treatment(follow_up, follow_up_parameters(duration(0,0,2,0,0,0)), "", low,"p17_tr_diag")
		])
	):- outcomeSpecification(fever_malaria_unlikely, _).


/* Initial goals */

!start.


/* Plans */

+!start <-

	// Register key feature data.
	for (feature(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY)) {
		.send(feature,tell,question(AGENT, FEATURE, QUESTION, HELP, IMAGE, PRIORITY));
	}
.

+!checkOutcome : not response(yes) & keysymptom(fever_malaria, yes)
		<-
		.perceive;
		if(keysymptom(fever_malaria, yes) & .count(response(yes),M) & M = 0){
			if(outcomeSpecification(DIAGNOSIS, _)){
				for (outcome(DIAGNOSE, TREATMENT)){
		 			.send(outcome, tell, outcome(DIAGNOSE, TREATMENT));
				}
				 +response(yes);
				.send(feature, tell, response(fever_malaria, yes));
			}else{
				.send(feature, tell, response(fever_malaria, no));
			}		
		}
.